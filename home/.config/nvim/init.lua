require("packer-config")

-- # File Explorer :
require("nvim-tree-config")

-- # Settings :
require("settings")

-- # Keymaps :
require("keymap-config")

-- # Autopairs :
require("autopairs-config")

-- # Tab bar :
require("tarbar-config")

-- # Indentation :
require("indentation-config")

-- # Startus Bar :
require("lualine-config")

-- # Treesitter :
require("treesitter-config")

-- # Telescope :
require("telescope-config")

-- # Toggleterm :
require("toggleterm-config")

-- # Startup :
require("startup-config")

-- # LSP :
require("lsp-config.language-servers")
require("lsp-config.lspsaga")
require("lsp-config.completion")
require("lsp-config.null-ls")

-- # Colorizer :
require("colorizer-config")

-- # Colorschames
require("colorschames.misery")
--require("colorschames.catppuccin")
