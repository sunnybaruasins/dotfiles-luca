return require("packer").startup(function()
	use("wbthomason/packer.nvim") --> packer plugin manager
	
	-- # File Explorer :
	use("nvim-tree/nvim-tree.lua")
	
	-- # Devicons :
	use("nvim-tree/nvim-web-devicons")
	
	-- # Tabs :
	use("romgrk/barbar.nvim")
	
	-- # Autopairs :
	use("windwp/nvim-autopairs")
	
	-- # Indent guides :
	use("lukas-reineke/indent-blankline.nvim")
	
	-- # Status Bar :
	use("nvim-lualine/lualine.nvim")
	
	-- # Treesitter :
	use({ "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }) --> treesitter
	use("nvim-treesitter/nvim-treesitter-textobjects") --> textobjects
	use("nvim-treesitter/nvim-treesitter-refactor")
	use("p00f/nvim-ts-rainbow")
	use("nvim-treesitter/playground")
	use("JoosepAlviste/nvim-ts-context-commentstring")
	
	-- # Telescope :
	use("nvim-lua/plenary.nvim")
	use("nvim-telescope/telescope.nvim")
	
	-- # Toggleterm :
	use("akinsho/toggleterm.nvim")
	
	-- # LSP :
	use("neovim/nvim-lspconfig") --> Collection of configurations for built-in LSP client
	use("williamboman/nvim-lsp-installer") --> Companion plugin for lsp-config, allows us to seamlesly install language servers
	use("jose-elias-alvarez/null-ls.nvim") --> inject lsp diagnistocs, formattings, code actions, and more ...
	use("tami5/lspsaga.nvim") --> icons for LSP diagnostics
	use("onsails/lspkind-nvim") --> vscode-like pictograms for neovim lsp completion items
	use("hrsh7th/nvim-cmp") --> Autocompletion plugin
	use("hrsh7th/cmp-nvim-lsp") --> LSP source for nvim-cmp
	use("saadparwaiz1/cmp_luasnip") --> Snippets source for nvim-cmp
	use("L3MON4D3/LuaSnip") --> Snippets plugin
	
	-- # Startup :
	use("startup-nvim/startup.nvim")
	
	-- # Colorizer :
	use("norcalli/nvim-colorizer.lua")
	
	-- # Colorscemes :
	--use "EdenEast/nightfox.nvim"
	use("navarasu/onedark.nvim")
	use("lucauxx/misery.nvim")
	use { "catppuccin/nvim", as = "catppuccin" }
	
end)
